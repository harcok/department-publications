# Bibtex repository for Software Science Department

This is a readonly repository, editing the bibtex entries can be done at:

> http://sws.cs.ru.nl/Bibtex/
